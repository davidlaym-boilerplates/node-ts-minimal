# Node.js and Typescript minimal project boilerplate

The objective of this template is to provide a quick start on Node.js for 
fast Proof of concepts, experiments or as base for more complicated templates
and sets the base tooling to grow to a real project.

## How to develop

- `npm run dev` : run the app and watch for file changes to restart
- `npm run tdd` : run the unit tests watching for file cahnges to restart them
- `npm run debug`: run the app and start the debugger
- `npm run test`: run style check and the whole unit test suite, reporting coverage
- `npm run lint`: run style check in the codebase
- `npm run build` : compile the app to one file in /dist
- `npm run start`: run the app compiled at /dist
- `npm run pre-commit`: runs the pre-commit check without the need for actually attempting a commit  

## How to deploy

After a successfull build, copy the `/dist` folder into the destination

## Technology Stack

- Typescript: Main language
- Jest: Unit test utility with coverage, library and mocks
- Prettier: style checker
- TSLint: code inspection
- ts-node-dev: development utility for watch and reload
- NCC: typescript compiler and bundler
