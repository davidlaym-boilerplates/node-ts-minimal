import { app } from './app'

describe('smoke test', () => {
  it('should return hello world ', () => {
    var result = app.run()
    expect(result).toBe('hello world')
  })
})
